\documentclass[%
  %draft
  %handout,dvipsnames,table
  final
 ]{beamer}

\usetheme[%
  sectionpage=none,
  numbering=fraction,
  progressbar=frametitle,
  block=fill
  ]{metropolis}

\usepackage{pgfpages}
\setbeameroption{%
 hide notes
 %show only notes
 %show notes on second screen
}

\usepackage{mathpazo} %Fontes palatino [http://www.ctan.org/tex-archive/fonts/mathpazo/]

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[brazil]{babel}
\usepackage{amsmath,amsthm,amssymb}
\usepackage{mathtools} %corrige bugs do pacote amsmath e adiciona outras melhorias [http://ctan.org/pkg/mathtools]
\usepackage{tensor} %Ver: http://ctan.org/tex-archive/macros/latex/contrib/tensor
\usepackage{color}

\usepackage{epsfig}
\usepackage{graphicx}
\usepackage[%
  backend=bibtex,
  citestyle=authoryear
  %bibliographystyle=authoryear-comp
  ]{biblatex}
\addbibresource{bibliografia}

\graphicspath{ {./img/} }





%% Definições de ambientes para teoremas, definições e similares
\theoremstyle{definition}
\newtheorem{definicao}{Definição}

\theoremstyle{plain}
\newtheorem{teorema}{Teorema}
\newtheorem{conjectura}{Conjectura}
\newtheorem{proposicao}{Proposição}
\newtheorem{lema}{Lema}
\newtheorem{corolario}{Corolário}

\theoremstyle{remark}
\newtheorem{observacao}{Observação}


%% Definição de comandos
\newcommand{\RR}{\mathbb{R}}
\newcommand{\til}[1]{\widetilde{#1}}
\renewcommand{\SS}{\mathbb{S}}


% Metadados da apresentação
\title[Massas GBC]
{\bf  Desigualdades Geométricas e o Tamanho de Corpos em Relatividade Geral}

\author[asm]
{
  Alexandre ``asm''
}
\institute[IM/UFAL]
{
  Instituto de Matemática\\
  Universidade Federal de Alagoas
}

\date[IM/UFAL 2018]{14 de setembro de 2018}


%% Início do conteúdo
\begin{document}

\begin{frame}
  \maketitle
  
  \note[item]{Inicialmente, gostaria de agradecer a presença de todas pessoas}
  \note[item]{Mencionar o patrocínio da CAPES e da FAMP
    (Fundação de Amparo Minha Poupança)}

\end{frame}


\begin{frame}
  \frametitle{Desigualdade isoperimétrica}
  Todo subconjunto \( \Omega \subset \RR^3 \), com \( \partial\Omega \) suave,
  satisfaz a desigualdade:
  \begin{equation*}
      |\Omega| \le {4\pi \over 3} \left( |\partial\Omega| \over 4\pi \right)^{3 \over 2}
    \end{equation*}

    Ademais,
    \begin{align*}
      |\Omega| = {4\pi \over 3} \left( |\partial\Omega| \over 4\pi \right)^{3 \over 2}
      \Longleftrightarrow \Omega = \mathbb{B}^3 \subset \RR^3
    \end{align*}

    \begin{block}{Tamanho mínimo}
      \begin{align*}
        \mathcal{R} \equiv \sqrt{|\partial\Omega| \over 4\pi}
        \quad \textrm{raio de área}
        \Longrightarrow |\Omega| \le {4\pi \over 3} \mathcal{R}^3
      \end{align*}
    \end{block} 
\end{frame}


\begin{frame}
  \frametitle{Conjectura}
  
  \begin{block}{\cite{DainBekensteinboundsinequalities2015} - \citetitle{DainBekensteinboundsinequalities2015}}
    \begin{align*}
      \frac{Q^4}{4} + J^2 \le k^2 \mathcal{R}^4
    \end{align*}

    Em unidades geométricas ( \( G = 1, c = 1\) ).
  \end{block}

  \begin{itemize}
  \item \( k \) denota uma \textbf{constante universal} adimensional \\
    (a determinar)
    \item \( Q \) denota a \textbf{carga elétrica}
    \item \( J \) denota o \textbf{momento angular}
    \item \( \mathcal{R} \) denota uma \textbf{medida do tamanho} com unidades
      de comprimento (a determinar)
  \end{itemize}

  \note{%
    \( Q \) e \( J \) são \textbf{quantidades conservadas}
  } 

  \note{%
    \begin{align*}
      \frac{Q^4}{4} + c^2 J^2 \le k^2 {c^8 \over G^2} \mathcal{R}^4
    \end{align*}
  }
\end{frame}


\begin{frame}
  \frametitle{Conjunto de dados iniciais em relatividade geral}
    Consiste de uma tupla \((M^n, g, K, \mu, j)\) que satisfaz as seguintes:
  \begin{alertblock}{equações de vínculos}
    \begin{align*}
      R_g + (\mathrm{tr}_gK)^2 -||K||^2_g &= 16\pi \mu \quad \textrm{(vínculo hamiltoniano)}\\
      \mathrm{div}_g \left( K - (\mathrm{tr}_g K)g \right) &= -8\pi j \quad \textrm{(vínculo do momento)}\\
      \mathcal{C}(\mathcal{F},g) &= 0 \quad \textrm{(vínculos da matéria)}
    \end{align*}
  \end{alertblock}

  \begin{itemize}
  \item \((M^3, g)\) é uma \textbf{variedade riemanniana};
  \item \( K \in \Gamma\left( S^2(T^{*}M) \right) \) é dita ser a
    \textbf{segunda forma fundamental};
  \item \(\mu \in C^{\infty}(M)\) denota a \textbf{densidade (pontual) de energia};
  \item \(j \in \Gamma\left( T^*M \right)\) denota a \textbf{densidade de momento};
  \item \(\mathcal{F}\) denota os \textbf{campos de matéria}.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Conjunto de dados iniciais em relatividade geral}
    Consiste de uma tupla \((M^n, g, K, \mu, j)\) que satisfaz as seguintes:
  \begin{alertblock}{equações de vínculos}
    \begin{align*}
      R_g + (\mathrm{tr}_gK)^2 -||K||^2_g &= 16\pi \mu \quad \textrm{(vínculo hamiltoniano)}\\
      \mathrm{div}_g \left( K - (\mathrm{tr}_g K)g \right) &= -8\pi j \quad \textrm{(vínculo do momento)}\\
      \mathcal{C}(\mathcal{F},g) &= 0 \quad \textrm{(vínculos da matéria)}
    \end{align*}
  \end{alertblock}

  Terminologia:
  \begin{itemize}
  \item \textbf{Conjunto de dados iniciais completo}: \((M^3, g)\) completa;
  \item Satisfaz a \textbf{condição da energia dominante}:
  \[ \mu \geq |j|_g; \]
  \item \textbf{Vácuo}: \(\mu \equiv 0\) e \(j \equiv 0\).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Campo eletromagnético}

  Consiste de uma seção
  \(\mathcal{F} \equiv \left( E, B, \rho \right) \in \Gamma\left( F \equiv TM \times TM \times \RR \right)\),
  que satisfaz as seguintes equações de vínculos:
  \begin{alertblock}{equações de Maxwell da eletrostática}
    \begin{align*}
      div_g E &= 4\pi\rho \\
      div_g B &= 0
    \end{align*}
  \end{alertblock}
  Equivalentemente:
  \[ \mathcal{C}(\mathcal{F},g) \equiv (div_gE - 4\pi\rho, div_gB) = 0. \]

  \begin{itemize}
  \item \( E \) denota o \textbf{campo elétrico};
  \item \( B \) denota o \textbf{campo magnético};
  \item \( \rho \) denota a \textbf{densidade de carga elétrica};
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Carga}
  \begin{block}{Carga (relativa) de uma superfície}
    \begin{align*}
      Q_X(\Sigma) := {1 \over 4\pi} \oint_{\Sigma} \langle{X, \nu}  \rangle d\sigma_g
    \end{align*}
  \end{block}

  \begin{itemize}
  \item \( \Sigma \hookrightarrow (M^3, g) \) denota uma superfície mergulhada
    e orientada;
  \item \( \nu \in \Gamma\left( UT\Sigma^{\perp} \right) \) denota o campo normal
    unitário exterior;
  \item \( X \in \Gamma\left( TM \right) \).
  \end{itemize}

  \centering{
    \( div_gX = 0 \Longrightarrow \) a carga é um \textbf{invariante homológico}.}
\end{frame}


\begin{frame}
  \frametitle{Carga}

  \( \Omega \subset M\) domínio compacto com \( \partial\Omega \) suave e orientada.
  \begin{block}{Carga magnética}
    \begin{align*}
      Q_B(\Omega) := Q_B(\partial\Omega)
      = {1 \over 4\pi} \oint_{\Sigma} \langle{B, \nu}  \rangle d\sigma_g
    \end{align*}
    é um invariante homológico.
  \end{block}

  \begin{block}{Carga elétrica}
    \begin{align*}
      Q_E(\Omega) := Q_E(\partial\Omega)
      = {1 \over 4\pi} \oint_{\Sigma} \langle{E, \nu}  \rangle d\sigma_g
    \end{align*}
    é um invariante homológico na condição de \textbf{matéria descarregada}
    \( \left( \rho \equiv 0 \right) \).
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{Abordagem heurística}
  \begin{block}{Cota de Bekenstein para a entropia
      \footcite{BekensteinBlackholepolarization1999}}

    \begin{align*}
      \frac{\hslash}{2\pi k_B} S \le \sqrt{(\mathcal{ER})^2 - J^2} - \frac{Q^2}{2}
    \end{align*}
  \end{block}

  \begin{itemize}
  \item \( \hslash \) denota a \textbf{constante de Planck reduzida} (positiva)
  \item \( k_B \) denota a \textbf{constante de Boltzman} (positiva)
  \item \( \mathcal{E} \) denota a \textbf{energia} do corpo
  \item \( S \) denota a \textbf{entropia} do corpo (\( S \ge 0 \), por definição)
  \end{itemize}
  %
  \begin{align*}
    S \ge 0 &\Longrightarrow \frac{Q^4}{4} + J^2 \le \mathcal{E}^2 \mathcal{R}^2 \\
    S \equiv 0 &\Longleftrightarrow \frac{Q^4}{4} + J^2  = \mathcal{E}^2 \mathcal{R}^2
    \quad \text{(rigidez)}
  \end{align*}
\end{frame}


\begin{frame}
  \frametitle{Abordagem heurística}
  \begin{block}{Conjectura do aro de Thorne}
    Sob a \textbf{condição da energia dominante}
    \begin{align*}
      \mathcal{E} > k\mathcal{R}
      \Longrightarrow \text{colapsa para um buraco negro}
    \end{align*}

    \centering{\( k \) é uma \textbf{constante universal}}
  \end{block}
  Logo, para um corpo ordinário\footnote{Isto é, que não está contido
    numa região de buraco negro.},
  \begin{align*}
    \mathcal{E} \le k\mathcal{R} &\Longrightarrow
      \color{red} \frac{Q^4}{4} + J^2  \le k^2 \mathcal{R}^4
  \end{align*}

  Ademais,
  \begin{align*}
  \frac{Q^4}{4} + J^2 = k^2 \mathcal{R}^4  \Longrightarrow S \equiv 0
  \end{align*}
\end{frame}


\begin{frame}
  \frametitle{Contra-exemplo para o raio de área}
  
\end{frame}


\begin{frame}
  \frametitle{Superfície isoperimétrica estável}
  
\end{frame}


\begin{frame}
  \frametitle{}
  \centering
  
  Obrigado!

\end{frame}
  
  
\end{document}